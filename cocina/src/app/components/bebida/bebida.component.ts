import { Component, OnInit,Input,HostBinding } from '@angular/core';
import {ItemsMenu } from '../../models/items-menu.model';

@Component({
  selector: 'app-bebida',
  templateUrl: './bebida.component.html',
  styleUrls: ['./bebida.component.css']
})
export class BebidaComponent implements OnInit {

  @Input() bebida: ItemsMenu;

  constructor() {
    
  }

  ngOnInit(): void {
  }

}
