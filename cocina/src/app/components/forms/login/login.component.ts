import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  mensajeError: string;

  constructor(public authService: AuthService,
    private router: Router) {
    this.mensajeError ='';
   }

  ngOnInit(): void {
  }

  login(u:string, p:string){
    this.mensajeError= '';
    if(!this.authService.login(u,p)){
      this.mensajeError= "Login incorrecto";
      setTimeout(function(){
        this.mensajeError= '';
      }.bind(this),2500);
    }
    this.router.navigateByUrl('/comanda');
    return false;
  }

  logout():boolean{
    this.authService.logout();
    this.router.navigateByUrl('/home');
    return false;
  }

}
