import { Component, EventEmitter, forwardRef, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, RequiredValidator, Validators } from '@angular/forms';
import { ItemsMenu } from '../../../models/items-menu.model';
import {ItemsApiClient } from '../../../models/items-api-client.model';
import { ValidadoresService } from "../../../services/validadores.service"
import { APP_CONFIG, AppConfig } from '../../../models/interfaces/app-config';

@Component({
  selector: 'app-form-comanda',
  templateUrl: './form-comanda.component.html',
  styleUrls: ['./form-comanda.component.css']
})
export class FormComandaComponent implements OnInit {

  @Output() onItemAdded: EventEmitter <ItemsMenu>;
  errNombre= 'Nombre necesario';
  errPrecio= 'Valor necesario y mayor que 2';
  public fg: FormGroup;
  items: ItemsMenu;
  index= 0;

  constructor(
    private fb: FormBuilder,
    private itemApi: ItemsApiClient,
    private validadores: ValidadoresService,
    @Inject(forwardRef(() => APP_CONFIG )) private config:AppConfig)  //forwardRed es apra evitar el bucle infinito de referencia circular.  
    {
      this.mkform();
      this.onItemAdded = new EventEmitter();
   }
   ngOnInit() {    
  }

   mkform(){
      this.fg= this.fb.group({
        id:[,],
        nombre:['',[Validators.required]],
        desc:['',],
        img:['',],
        precio:['',[Validators.required, this.validadores.noZero]], //Validador de requerido y personalizado de mayor de 1
        seleccion:[false,],
        fav:[false,]
      }),
      this.fg.valueChanges.subscribe((form: any)=>{
        console.log("Cambios en form.");
      },
      )
   }
  
   error(e:string):boolean{

     if (this.fg.controls[e].hasError('required')){
       return true;

     }if(this.fg.controls[e].hasError('required'&&'noZero')){
      return true;
     }
   }


  guardar()  {   
    this.index=this.itemApi.getId(); //Cambiamos el valor del index..
    this.fg.patchValue({id: this.index}); //Parcheando el valor porque 
    this.items = this.fg.value;  //PAsamos el objeto a items para enviarlo a la api.
    this.onItemAdded.emit(this.items);
    return true;
  }


}
