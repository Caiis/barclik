import { Component, OnInit } from '@angular/core';
import {ItemsApiClient } from '../../models/items-api-client.model';
import {ItemsMenu } from '../../models/items-menu.model';

@Component({
  selector: 'app-detalle-item',
  templateUrl: './detalle-item.component.html',
  styleUrls: ['./detalle-item.component.css'],
  providers: [ItemsApiClient]
})
export class DetalleItemComponent implements OnInit {
  item: ItemsMenu;
  style= {
    sources:{
      world:{
          type: 'geojson',
          data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
      }
    },
    version:8,
    layers:[{
      'id': 'countries',
      'type': 'fill',
      'source': 'world',
      'layout': {},
      'paint':{
        'fill-color': '#6F788A'
      }
    }]
  };
  constructor() { }

  ngOnInit(): void {
  }

}
