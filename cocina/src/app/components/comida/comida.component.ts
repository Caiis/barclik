import { Component, OnInit,Input,HostBinding, Output, EventEmitter } from '@angular/core';
import { ItemsMenu } from '../../models/items-menu.model';
import { ItemsApiClient } from '../../models/items-api-client.model';
import { trigger,state, style,transition, animate } from '@angular/animations'

@Component({
  selector: 'app-comida',
  templateUrl: './comida.component.html',
  styleUrls: ['./comida.component.css'],
  animations: [
    trigger('esFavorito',[
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito',[
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito',[
        animate('1s')
      ]),
    ])
  ]

  
  
})
export class ComidaComponent implements OnInit {

  @Input() plato:ItemsMenu;
  @HostBinding('attr.class') cssClass='col-md-4';
  @Output() clicked: EventEmitter<ItemsMenu>;
  @Output() fav: EventEmitter<ItemsMenu>;
  apiItems: ItemsApiClient;

  
  constructor() {
    this.clicked = new EventEmitter();
    this.fav = new EventEmitter();
  }

  ngOnInit(): void {
  }

  elegir(){
    this.clicked.emit(this.plato);
    return false;
  }
  quitar(){
    this.clicked.emit(this.plato);
    return false;
  }
  favorito(){
    this.fav.emit(this.plato);
    return false;

  }
 

}
