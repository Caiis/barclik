import { Component, OnInit,Input, Output, EventEmitter, Inject, forwardRef, InjectionToken, Injectable } from '@angular/core';
import {ItemsMenu } from '../../models/items-menu.model';
import { ComidaApiService } from '../../services/comida-api.service';
import {ItemsApiClient } from '../../models/items-api-client.model';
import { map, filter,debounceTime,distinctUntilChanged,switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { fromEvent } from 'rxjs';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AppConfig,APP_CONFIG_VALUE,APP_CONFIG } from '../../models/interfaces/app-config';


const tipoPlato = new InjectionToken<ItemsMenu>('tipo.plato');

class ItemsMenuDecorated extends ComidaApiService {
  constructor ( ) {
    console.log("Llamado por la clase decorada! ");
      super();
  }
}


@Component({
  selector: 'app-comanda',
  templateUrl: './comanda.component.html',
  styleUrls: ['./comanda.component.css'],
  providers: [  
    {provide: ComidaApiService, useClass: ItemsMenuDecorated},
    {provide: ItemsMenu, useExisting: ComidaApiService},
  ]
})

export class ComandaComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<ItemsMenu>;
  updates: string[];

  //Para la busqueda hay que tener un div con lista para que me muestre la lista de busqueda.
  public fg: FormGroup;
  searchResults: string[];  //Para almacenar la busqueda.


  constructor(
    public comApi: ItemsApiClient,
    private fb: FormBuilder,
    @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig
    ) 
    { 
      // this.comApi.getAll();
      this.onItemAdded = new EventEmitter();  
      this.updates= [];
      this.mkfomr();

      this.comApi.subscribeOnChange((i: ItemsMenu)=> {
        if( i!= null){
          this.updates.push('Ha sido favorito'+i.nombre);
        }
      }
      );
  }
mkfomr(){
  this.fg= this.fb.group({
    search:['']
  });
}


  ngOnInit() {  
    this.comApi.getPlato().subscribe(res=>{
    })

    //Ya no es necesario al tener un json en el backend.
    // this.comApi.getAll();
    //Esto es para hacer la busqueda en cualquier sitio. Busca sobre la palabra vinvculada al FORMULARIO

    let elemNombre = <HTMLInputElement>document.getElementById('search');

    fromEvent(elemNombre, 'input')
      .pipe(  //Para hacer varias busquedas a la vez
        map((e: KeyboardEvent)=> (e.currentTarget as HTMLInputElement).value),  //Coge los resultados del teclado por el valor de htmlinput
        filter(text => text.length > 2), //Filtro sobre como minimo  2 caracteres empiece a buscar
        debounceTime(300), //tiempo de espera hasta hacer la proxima busqueda pos si teclean muy rapido
        distinctUntilChanged(), //Si hay algun cambio despues de los 300 ms, ejemplo si ponemos letra y borramos antes no cambiara nada.
        switchMap((text:String) => ajax(this.config.apiEndPoint + '/ciudades?q='+text)) //Es sobre donde vamos a hacer la busqueda. 
      ).subscribe( ajaxResponse => {  //un callback con los resultaros y añadiendolos a searchResults
        this.searchResults = ajaxResponse.response;
        
      });
      
  }

  agregado(p: ItemsMenu){
    this.comApi.add(p);
  }
  

  elegido(i: ItemsMenu){
    this.comApi.elegir(i);
  }
  
  favorito(i: ItemsMenu){
    this.comApi.selectFav(i);
  }

  selectById(id:number){
    this.comApi.getById(id);
    console.log("Este es el objeto seleccionaro.");
  }

}
