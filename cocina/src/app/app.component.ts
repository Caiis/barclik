import { Component } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Cocina';
  //Haciendo un reloj con observable.
  time = new Observable( observer => {// variable que le creamos un observable con una variable observer a la cual se subscribe para el proximo evento 
    setInterval(() => observer.next(new Date().toString()), 1000); //Tiempo que esperar para llevar al evento proximo y asi cambiarla la hora.    --- Asegurarme que es Async que esta en el template del componente este junto un pipe y la cariable time.6yhj
  });
}
