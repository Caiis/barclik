
    let elemNombre = <HTMLInputElement>document.getElementById('nombres');

    fromEvent(elemNombre, 'input')
      .pipe(  //Para hacer varias busquedas a la vez
        //map((e: KeyboardEvent) => (<HTMLInputElement>e.target).value),
        map((e: KeyboardEvent)=> (e.currentTarget as HTMLInputElement).value),  //Coge los resultados del teclado por el valor de htmlinput
        filter(text => text.length > 2), //Filtro sobre como minimo  2 caracteres empiece a buscar
        debounceTime(300), //tiempo de espera hasta hacer la proxima busqueda pos si teclean muy rapido
        distinctUntilChanged(), //Si hay algun cambio despues de los 300 ms, ejemplo si ponemos letra y borramos antes no cambiara nada.
        switchMap((text:String) => ajax('/assets/datos.json')) //Es sobre donde vamos a hacer la busqueda. 
      ).subscribe( ajaxResponse => {  //un callback con los resultaros y añadiendolos a searchResults
        this.searchResults = ajaxResponse.response;
        
      });