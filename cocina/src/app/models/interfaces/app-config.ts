import { InjectionToken, NgModule } from '@angular/core';

export interface AppConfig {
    apiEndPoint: 'http://localhost:3000';
    title:'Camarero';
  }
  
  export const APP_CONFIG_VALUE: AppConfig = {
    apiEndPoint: 'http://localhost:3000',
    title: 'Camarero'
  };
  
  export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');