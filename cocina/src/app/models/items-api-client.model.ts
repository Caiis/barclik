import { HttpClient, HttpHeaders, HttpRequest, HttpResponse, } from '@angular/common/http';
import { BehaviorSubject, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import {ItemsMenu } from '../models/items-menu.model';
import {APP_CONFIG, AppConfig,APP_CONFIG_VALUE } from '../models/interfaces/app-config';
import { forwardRef, Inject, Injectable } from '@angular/core';
import { db, MyDatabaseService } from '../services/my-database.service';

@Injectable({
    providedIn: 'root'
  })
export class ItemsApiClient{
    items: ItemsMenu[];
    current: Subject<ItemsMenu> = new BehaviorSubject<ItemsMenu>(null);
       
    constructor(
         @Inject (forwardRef(() => APP_CONFIG)) private config: AppConfig,
        private http:HttpClient,
        
        ){
            this.items=[];             
    }

    getAll(){
        // console.log("1-API Get All ");
        // const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN':'token-seguridad'});
        // const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndPoint + '/items');

        
        return this.items;
    }
    getPlato(){

        return this.http.get(this.config.apiEndPoint + '/items')
        .pipe(
            map(res => this.crearArreglo(res) )
        )
    }

    private crearArreglo(itemsObj: Object){
        if(itemsObj=== null){
            return[];
        }
        Object.keys(itemsObj).forEach(key =>{
            console.log("Asi vemos la respuesta: "+itemsObj[key]);
            this.items[key] = itemsObj[key];
            
        })
        return 'test Arreglando';
    }

    // add(i: ItemsMenu){
    //     console.log("El EndPooint es: "+this.config.apiEndPoint);
    //     return this.http.post(this.config.apiEndPoint+'/addItems',i);
    // }
    add(i: ItemsMenu){
        console.log("El EndPooint es: "+APP_CONFIG_VALUE.apiEndPoint);
        const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN':'token-seguridad'});
        const req = new HttpRequest('POST', APP_CONFIG_VALUE.apiEndPoint + '/addItems', 
        {nuevo: i},         
         {headers: headers});
        this.http.request(req).subscribe((data:HttpResponse<{}>) => {
            console.log("Estoy dentro del CallBack Resquest")
            if(data.status ===200) {
                this.items.push(i);
                const myDb = db;
                myDb.items.add(i);
                console.log("Mostrando todos los platos de la DB");
                myDb.items.toArray().then(items => console.log(items))
                
            }
        });
    }
    // add(i: ItemsMenu){
    //     console.log("El EndPooint es: "+APP_CONFIG_VALUE.apiEndPoint);
    //     const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN':'token-seguridad'});
    //     const req = new HttpRequest('POST', APP_CONFIG_VALUE.apiEndPoint + '/addItems', {nuevo: i.nombre}, {headers: headers});
    //     this.http.request(req).subscribe((data:HttpResponse<{}>) => {
    //         console.log("Estoy dentro del CallBack Resquest")
    //         if(data.status ===200) {
    //             this.items.push(i);
                
    //         }
    //     })
    // }
    // add(i: ItemsMenu){      
    //     this.items.push(i);
    // }

    getId():number{
          return this.items.length;
    }
    
    elegir(i:ItemsMenu){
        if (i.seleccion == false){    
            i.seleccion=true;                
        }else{
            i.seleccion=false;
        }
    }

    selectFav(i:ItemsMenu){
        this.items.forEach( x => x.fav = false);
        i.fav = true;
        this.current.next(i);
    }

    getById(id:number):ItemsMenu {
        console.log("He recibido el id: "+id)
        return this.items[id];
    }

    subscribeOnChange(fn){
        this.current.subscribe(fn);
    }
    testUser():boolean{
        return localStorage.getItem('usuario') !== null;
    }

}