import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })
  
export class ItemsMenu{
    public salsas: string[];
    constructor(
        public id: number,
        public nombre:string, 
        public desc:string, 
        public imagenUrl: string, 
        public precio: number,
        public seleccion :boolean,
        public fav: boolean
        ){ 
            this.salsas= ['ketchup', 'mahonesa', 'agridulce','bbq'];
        }
}
