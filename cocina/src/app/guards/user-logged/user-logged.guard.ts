import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserLoggedGuard implements CanActivate {
  constructor(private authService: AuthService){}

  canActivate(
    route: ActivatedRouteSnapshot,  //Al siguiente elemento que queremos ir .. si el estado no esta logeado se activara el guard.
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean  {
      const isLogged = this.authService.isLogged();
      console.log("canActivate", isLogged);
      return isLogged;
  }
  
}
