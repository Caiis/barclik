import { Directive, ElementRef } from '@angular/core';
import { fromEvent } from 'rxjs';

@Directive({
  selector: '[appTrackearClick]'
})
export class TrackearClickDirective {
  private element: HTMLInputElement;

  //Recibimos por injeccion de dependencias como la directiva 
  //se aplica sobre un elemento llamado elRef, que puede ser un div, un button, un a... 
  //Gracias a estopodemos crear con programacion reactiva(subscribe) que cada vez que 
  //en este caso hagan click, en el evento ('click')
  //llamamos a la funcion Track. En la funcion TRack
  //comprobamos si tiene un atributo llamado data-trackear-tags
  //Aqui como prueba lo logeamos en consola, pero normalemnte se almacena.
  
  //Con ElementRef es como conocemos la referencia a ese compoennte HTML
  //

  //Lo vamos a cargar en comidaComponent cuando hagan click en favorito..
  constructor(private elRef: ElementRef) {  
    this.element = elRef.nativeElement;
    fromEvent(this.element, 'click')
    .subscribe(evento => this.track(evento));
   }
   track(evento:Event){
     const elemTags = this.element
     .attributes
     .getNamedItem('data-trackear-tags')
     .value
     .split(' '); //Con esto hace que cada espacio lo separe con comas, es decir lo estamos convirtiendo en array...
     console.log(`|||||||| track evento: ${elemTags}`);

   }

}
