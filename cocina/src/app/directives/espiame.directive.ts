import { Directive, OnInit, OnDestroy } from '@angular/core';

@Directive({
  selector: '[appEspiame]'
})

////Directiva para ver cuando se cargan y se destruyen los componentes. 
//Esta activa en 
export class EspiameDirective  implements OnInit, OnDestroy{
  static nextId=0;

  log= (msg: string) => console.log(`Evento #${EspiameDirective.nextId++} ${msg}`);
  ngOnInit(){
    this.log('#########*********onInit')
  }
  ngOnDestroy(){
    this.log('#########*********OnDestroy')
  }

  constructor() { }

}
