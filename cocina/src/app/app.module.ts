import { BrowserModule } from '@angular/platform-browser';
import { InjectionToken, NgModule } from '@angular/core';
import { RouterModule,Routes } from '@angular/router';
import { StoreModule as NgRxStoreModule, ActionReducerMap } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'

import { AppComponent } from './app.component';
import { ComandaComponent } from './components/comanda/comanda.component';
import { ComidaComponent } from './components/comida/comida.component';
import { BebidaComponent } from './components/bebida/bebida.component';
import { DetalleItemComponent } from './components/detalle-item/detalle-item.component';
import { DetalleComidaComponent } from './components/detalle-comida/detalle-comida.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormComandaComponent } from './components/forms/form-comanda/form-comanda.component';
import {ItemsApiClient } from './models/items-api-client.model';
import { ValidadoresService } from "./services/validadores.service"
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/forms/login/login.component';
import { MenuComponent } from './components/protegido/menu/menu.component';
import { UserLoggedGuard } from './guards/user-logged/user-logged.guard';
import { AuthService } from './services/auth.service';
import { APP_CONFIG, APP_CONFIG_VALUE } from './models/interfaces/app-config';
import { HttpClientModule } from '@angular/common/http'
import { MyDatabaseService } from './services/my-database.service';
import { NgxMapboxGLModule, MapService } from 'ngx-mapbox-gl';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EspiameDirective } from './directives/espiame.directive';
import { TrackearClickDirective } from './directives/trackear-click.directive';
//import {   ItemMenuState,   reducerItemMenu ,  initializeItemsMenuState,  ItemxMenuEffects} from './services/comanda-items-state.model';


const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'platos', component: DetalleItemComponent},
  {
  path: 'comanda', 
  component: ComandaComponent,
  canActivate: [UserLoggedGuard]
  },
  {
    path: 'menu',
    component: MenuComponent,
    canActivate: [UserLoggedGuard]
  }
]

//redux init
//export interface AppState{
//  platos: ItemsMenu;
//};

//const reducers: ActionReducerMap<AppState> = {
//  items: reducerItemMenu
//};
//let reducersInitialState= {
//  items: initializeItemsMenuState()
//};


@NgModule({
  declarations: [
    AppComponent,
    ComandaComponent,
    ComidaComponent,
    BebidaComponent,
    DetalleItemComponent,
    DetalleComidaComponent,
    FormComandaComponent,
    HomeComponent,
    LoginComponent,
    MenuComponent,
    EspiameDirective,
    TrackearClickDirective,
    
    
  ],
  imports: [
    BrowserModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    NgxMapboxGLModule,
    BrowserAnimationsModule,
  //  NgRxStoreModule.forRoot(reducers, {initialState: reducersInitialState}),
  //  EffectsModule.forRoot([ItemsMenuEffects])

  ],
  providers: [
    ItemsApiClient,
    ValidadoresService,
    AuthService,
    UserLoggedGuard,
    MyDatabaseService,
    MapService,
    {provide : APP_CONFIG, useValue: APP_CONFIG_VALUE}
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
