import { Injectable } from '@angular/core';
import Dexie from 'dexie';
import {ItemsMenu } from '../models/items-menu.model';

@Injectable({
  providedIn: 'root'
})
export class MyDatabaseService extends Dexie{
  items: Dexie.Table<ItemsMenu, number>;
  constructor() {
    super('MyDatabase');
    this.version(1).stores({
      items: '++id, nombre, desc, imagenUrl, precio, seleccion, fav'
    })
   }
}
export const db = new MyDatabaseService();