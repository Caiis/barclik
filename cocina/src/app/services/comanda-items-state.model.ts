//import { Action } from 'rxjs/internal/scheduler/Action';
import { ItemsMenu } from '../models/items-menu.model';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { state } from '@angular/animations';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';



//ESTADO
export interface ItemMenuState {
    items: ItemsMenu[];
    loading: boolean;
    favorito: ItemsMenu;

}
export const initializeItemsMenuState = function(){
    return {
        items:[],
        loading: false,
        favorito: null
    }
}

//ACCIONES
//Aqui se guarda toda la info que son strings
export enum ItemsMenuActionTypes{
    NUEVO_ITEM = '[Items Menu] Nuevo',
    ELEGIDO_FAVORITO = '[Items Menu] Favorito'
}

export class NuevItemAction implements Action {
    type = ItemsMenuActionTypes.NUEVO_ITEM;
    constructor(public item:ItemsMenu){}
}
export class ElegidoFavoritoAction implements Action {
    type = ItemsMenuActionTypes.ELEGIDO_FAVORITO;
    constructor(public item:ItemsMenu){}
}

export type ItemsMenuActions = NuevItemAction | ElegidoFavoritoAction;

//REDUCERS
export function reducerItemMenu(
    state: ItemMenuState,
    action: ItemsMenuActions
):ItemMenuState{
    switch(action.type) {
        case ItemsMenuActionTypes.NUEVO_ITEM: {
            return{ 
            ...state,
            items: [...state.items], (action as NuevItemAction).item]
            };
        }
        case ItemsMenuActionTypes.ELEGIDO_FAVORITO: {
            state.items.forEach(x => { x.setSelected(false));
                const fav: ItemsMenu = (action as ElegidoFavoritoAction).item;
                fav.setSelected(true);
                return {
                    ...state,
                    favorito: fav
                };   
            }
        }
        return state;
    }
}

//EFFECTS
@Injectable()
export class ItemxMenuEffects{
    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(ItemsMenuActionTypes.NUEVO_ITEM),
        map((action:NuevItemAction)=>new ElegidoFavoritoAction(action.item))
    );

    constructor(private actions$: Actions){

    }

}