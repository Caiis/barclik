import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  login(u:string, p:string){
    if(u ==='usuario' && p ==='password'){
      localStorage.setItem('username', u);
      return true;
    }else {
      return false;
    }

  }
  logout(){
    localStorage.removeItem('username');
  }

  isLogged():boolean{
    return this.getUser() !==null;
  }
  getUser():any{
    return localStorage.getItem('username');
  }

}


