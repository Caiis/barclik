import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidadoresService {

  info: string;
  constructor() { }

  noZero(control: FormControl): {[s:string]:boolean}{
      if(control.value < 2  ){
        return{
        noZero: true
        }
    }
    return null;
  }
  muestraErrores(n:number):string{

    switch (n){
      case 0:{
         return'Nombre necesario';
         break;
      }
      case 1:{
        return 'Valor necesario y mayor que 2';
        break;
      }
      case 2:{
        return'Valor necesario y mayor que 2'
        break;
      }
      default: {

      }
    }
    return 'hola';
  }
}
