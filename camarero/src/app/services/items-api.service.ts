import { Injectable } from '@angular/core';
import { ItemsMenu } from '../models/items-menu.models';

@Injectable({
  providedIn: 'root'
})
export class ItemsApiService {
  items: ItemsMenu[];
  index: number;
  constructor() { 
    this.items=[];

  }

  getAll(){
    return this.items;
  }

  addItem(i: ItemsMenu){
    this.addIndex
    i.id=this.index;
    this.items.push(i);
    console.log("Se ha añadido en Api Array Id: "+i.id);
    console.log("Se ha añadido en Api Array Indice: ")
  }
  leeIndex():number{
    this.index= this.items.length;
    if (this.index == null) {
      this.index = 0;
    }
    console.log('Leyendo Indice en Api: '+this.index)
    return this.index;
  }
  addIndex(i:ItemsMenu){
     this.index = this.items.length;
     return this.index;
  }
}
