import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { ComandaComponent } from './pages/comanda/comanda.component';
import { FormComidaComponent } from './forms/form-comida/form-comida.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PlatoComponent } from './templates/plato/plato.component';
import { MenuComponent } from './pages/menu/menu.component';
import { PlatosComponent } from './pages/platos/platos.component';
import  { ItemsApiService } from '../app/services/items-api.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ComandaComponent,
    FormComidaComponent,
    PlatoComponent,
    MenuComponent,
    PlatosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    ItemsApiService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
