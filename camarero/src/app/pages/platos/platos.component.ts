import { Component, OnInit,Output, EventEmitter, HostBinding, Input } from '@angular/core';
import { ItemsMenu } from '../../models/items-menu.models';

@Component({
  selector: 'app-platos',
  templateUrl: './platos.component.html',
  styleUrls: ['./platos.component.css']
})
export class PlatosComponent implements OnInit {
  @HostBinding('attr.class') cssClass='col-md-4';
  @Input() plato: ItemsMenu;  //Para pintar los datos de los platos desde menu, sino estarian sin info.
  constructor() { }

  ngOnInit(): void {
  }

}
