import { Component, OnInit } from '@angular/core';
import { ItemsMenu } from '../../models/items-menu.models';
import { ItemsApiService } from 'src/app/services/items-api.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})

export class MenuComponent implements OnInit {

  plato: ItemsMenu;
  platos: ItemsMenu[];

  constructor(
    private itemApi: ItemsApiService,
  ) { 
    
  }

  ngOnInit(): void {
    this.platos=this.itemApi.getAll();
  }
  

}
