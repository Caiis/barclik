import { Component, OnInit } from '@angular/core';
import { ItemsApiService } from 'src/app/services/items-api.service'
import { ItemsMenu } from '../../models/items-menu.models';
import { from } from 'rxjs';

@Component({
  selector: 'app-comanda',
  templateUrl: './comanda.component.html',
  styleUrls: ['./comanda.component.css']
})
export class ComandaComponent implements OnInit {
  
  item: ItemsMenu;
  constructor(
      private itemApi : ItemsApiService
  ) { }

  ngOnInit(): void {
  }

  addPlato(i: ItemsMenu){
    this.itemApi.addItem(i);
  }



}
