import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ItemsMenu } from '../../models/items-menu.models';
import { ItemsApiService } from 'src/app/services/items-api.service'
import { from } from 'rxjs';

@Component({
  selector: 'app-form-comida',
  templateUrl: './form-comida.component.html',
  styleUrls: ['./form-comida.component.css']
})
export class FormComidaComponent implements OnInit {
  
  public formGroup: FormGroup;
  comida: ItemsMenu;
  index: number ;
  
  @Output() addComida: EventEmitter <ItemsMenu>;

  constructor(
    private fb: FormBuilder,
    private itemApi: ItemsApiService
  ) {
    this.mkform();
    this.addComida = new EventEmitter();    
   }

  ngOnInit(): void {
  }

  mkform(){
    
    //Construccion del Formulario, datos two binding y Validadores.
    this.formGroup= this.fb.group({
      id:[this.index,],
      nombre:[, ],
      desc:[,],
      img:[,],
      precio:[,],
      seleccion:[false,],
    });
 }

 guardar(){
   console.log("Lee indice en formulario: "+this.index);
  this.index = this.itemApi.leeIndex();
  this.comida = this.formGroup.value
  console.log("Estamos en Form Guardar");
  this.addComida.emit(this.formGroup.value);
 }

}
